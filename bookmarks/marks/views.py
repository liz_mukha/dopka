from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views import View

from .models import Post
from .models import User
from marks.forms import PostForm


class Index(View):
    def get(self, request):
        context = {'text': 'Hello, Django!'}
        return render(request, 'base.html', context)


class PostPost(View):
    def post(self, request, username):
        form = PostForm(self.request.POST)
        if form.is_valid():
            user = User.objects.get(username=username)
            post = Post(text=form.cleaned_data['text'], user=user)
            post.save()
        return HttpResponseRedirect('/user/'+username)


class Profile(View):

    def get(self, request, username):
        user = User.objects.get(username=username)
        posts = Post.objects.filter(user=user)
        form = PostForm()
        context = {
            'posts': posts,
            'user': user,
            'form': form,
        }
        return render(request, 'profile.html', context)
