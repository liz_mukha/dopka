from django import forms


class PostForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea(attrs={
        'rows': 3,
        'class': 'form-control',
        'placeholder': 'Write your post here'
    }), max_length=500)
